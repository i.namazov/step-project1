const _GALLERY_ITEM = [[ ], [
        {
            imgSrc: "img/graphicDesign/graphic-design1.jpg",
            title: "Creative Design",
            category: "Graphic Design"
        }, {
            imgSrc: "img/graphicDesign/graphic-design2.jpg",
            title: "Creative Design",
            category: "Graphic Design"
        }, {
            imgSrc: "img/graphicDesign/graphic-design3.jpg",
            title: "Creative Design",
            category: "Graphic Design"
        }, {
            imgSrc: "img/graphicDesign/graphic-design4.jpg",
            title: "Creative Design",
            category: "Graphic Design"
        }, {
            imgSrc: "img/graphicDesign/graphic-design5.jpg",
            title: "Creative Design",
            category: "Graphic Design"
        }, {
            imgSrc: "img/graphicDesign/graphic-design6.jpg",
            title: "Creative Design",
            category: "Graphic Design"
        }, {
            imgSrc: "img/graphicDesign/graphic-design7.jpg",
            title: "Creative Design",
            category: "Graphic Design"
        }, {
            imgSrc: "img/graphicDesign/graphic-design8.jpg",
            title: "Creative Design",
            category: "Graphic Design"
        }, {
            imgSrc: "img/graphicDesign/graphic-design9.jpg",
            title: "Creative Design",
            category: "Graphic Design"
        }, {
            imgSrc: "img/graphicDesign/graphic-design10.jpg",
            title: "Creative Design",
            category: "Graphic Design"
        }, {
            imgSrc: "img/graphicDesign/graphic-design11.jpg",
            title: "Creative Design",
            category: "Graphic Design"
        }, {
            imgSrc: "img/graphicDesign/graphic-design12.jpg",
            title: "Creative Design",
            category: "Graphic Design"
        },
    ],
     [ {
        imgSrc: "img/web%20design/web-design1.jpg",
        title: "Creative Design",
        category: "Web  Design"
    },{
        imgSrc: "img/web%20design/web-design2.jpg",
         title: "Creative Design",
        category: "Web  Design"
    },{
        imgSrc: "img/web%20design/web-design3.jpg",
         title: "Creative Design",
        category: "Web  Design"
    },{
        imgSrc: "img/web%20design/web-design4.jpg",
         title: "Creative Design",
        category: "Web  Design"
    },{
        imgSrc: "img/web%20design/web-design5.jpg",
         title: "Creative Design",
        category: "Web  Design"
    },{
        imgSrc: "img/web%20design/web-design6.jpg",
         title: "Creative Design",
        category: "Web  Design"
    },{
        imgSrc: "img/web%20design/web-design7.jpg",
         title: "Creative Design",
        category: "Web  Design"
    },],
    _LANDING_PAGES = [{
        imgSrc: "img/landingPage/landing-page1.jpg",
        title: "Creative Design",
        category: "Landing Pages"
    },{
        imgSrc: "img/landingPage/landing-page2.jpg",
        title: "Creative Design",
        category: "Landing Pages"
    },{
        imgSrc: "img/landingPage/landing-page3.jpg",
        title: "Creative Design",
        category: "Landing Pages"
    },{
        imgSrc: "img/landingPage/landing-page4.jpg",
        title: "Creative Design",
        category: "Landing Pages"
    },{
        imgSrc: "img/landingPage/landing-page5.jpg",
        title: "Creative Design",
        category: "Landing Pages"
    },{
        imgSrc: "img/landingPage/landing-page6.jpg",
        title: "Creative Design",
        category: "Landing Pages"
    },{
        imgSrc: "img/landingPage/landing-page7.jpg",
        title: "Creative Design",
        category: "Landing Pages"
    }],
     [{
        imgSrc: "img/wordpress/wordpress2.jpg",
         title: "Creative Design",
        category: "Wordpress"
    },{
        imgSrc: "img/wordpress/wordpress3.jpg",
         title: "Creative Design",
        category: "Wordpress"
    },{
        imgSrc: "img/wordpress/wordpress4.jpg",
         title: "Creative Design",
        category: "Wordpress"
    },{
        imgSrc: "img/wordpress/wordpress5.jpg",
         title: "Creative Design",
        category: "Wordpress"
    },{
        imgSrc: "img/wordpress/wordpress6.jpg",
         title: "Creative Design",
        category: "Wordpress"
    },{
        imgSrc: "img/wordpress/wordpress7.jpg",
         title: "Creative Design",
        category: "Wordpress"
    },{
        imgSrc: "img/wordpress/wordpress8.jpg",
         title: "Creative Design",
        category: "Wordpress"
    },{
        imgSrc: "img/wordpress/wordpress9.jpg",
         title: "Creative Design",
        category: "Wordpress"
    },{
        imgSrc: "img/wordpress/wordpress10.jpg",
         title: "Creative Design",

         category: "Wordpress"
    }]];

const _ARTICLE_CONTENT = [
    {
        imgSrc: 'img/web%20design/web-design4.jpg',
        title: "web_services",
        text: " Web Services , sapiente veritatis.Adipisci at blanditiis deleniti quam quasi quis repellat reprehenderit ullam! Beatae ipsa, modi molestiae obcaecati placeat quod quos temporibus vero! Dolorum error, maxime molestiae non quaerat repudiandae tempora unde velit.Aliquid aperiam architecto autem distinctio ducimus illum inventore nulla optio, pariatur perspiciatis possimus praesentium quia quod quos reprehenderit voluptates voluptatum. Adipisci culpa eos libero maxime odio omnis perferendis possimus saepe.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa eos eveniet, ex, facilis laudantium libero omnis perspiciatis, quam quasi quibusdam quis quod sunt voluptate. Dignissimos error eum harum id molestias."},
    {
        imgSrc: "img/graphicDesign/graphic-design4.jpg",
        title: "graphic_design",
        text: "  Graphic design  , sapiente veritatis.Adipisci at blanditiis deleniti quam quasi quis repellat reprehenderit ullam! Beatae ipsa, modi molestiae obcaecati placeat quod quos temporibus vero! Dolorum error, maxime molestiae non quaerat repudiandae tempora unde velit.Aliquid aperiam architecto autem distinctio ducimus illum inventore nulla optio, pariatur perspiciatis possimus praesentium quia quod quos reprehenderit voluptates voluptatum. Adipisci culpa eos libero maxime odio omnis perferendis possimus saepe.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa eos eveniet, ex, facilis laudantium libero omnis perspiciatis, quam quasi quibusdam quis quod sunt voluptate. Dignissimos error eum harum id molestias."},
    {
        imgSrc: "img/landingPage/landing-page6.jpg",
        title: "online_support",
        text: " Online Support , sapiente veritatis.Adipisci at blanditiis deleniti quam quasi quis repellat reprehenderit ullam! Beatae ipsa, modi molestiae obcaecati placeat quod quos temporibus vero! Dolorum error, maxime molestiae non quaerat repudiandae tempora unde velit.Aliquid aperiam architecto autem distinctio ducimus illum inventore nulla optio, pariatur perspiciatis possimus praesentium quia quod quos reprehenderit voluptates voluptatum. Adipisci culpa eos libero maxime odio omnis perferendis possimus saepe.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa eos eveniet, ex, facilis laudantium libero omnis perspiciatis, quam quasi quibusdam quis quod sunt voluptate. Dignissimos error eum harum id molestias."},
    {
        imgSrc: "img/web%20design/web-design4.jpg",
        title: "app_design",
        text: " App Design  , sapiente veritatis.Adipisci at blanditiis deleniti quam quasi quis repellat reprehenderit ullam! Beatae ipsa, modi molestiae obcaecati placeat quod quos temporibus vero! Dolorum error, maxime molestiae non quaerat repudiandae tempora unde velit.Aliquid aperiam architecto autem distinctio ducimus illum inventore nulla optio, pariatur perspiciatis possimus praesentium quia quod quos reprehenderit voluptates voluptatum. Adipisci culpa eos libero maxime odio omnis perferendis possimus saepe.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa eos eveniet, ex, facilis laudantium libero omnis perspiciatis, quam quasi quibusdam quis quod sunt voluptate. Dignissimos error eum harum id molestias."},
    {
        imgSrc: "img/graphicDesign/graphic-design1.jpg",
        title: "online_marketing",
        text: " online  Marketing , sapiente veritatis.Adipisci at blanditiis deleniti quam quasi quis repellat reprehenderit ullam! Beatae ipsa, modi molestiae obcaecati placeat quod quos temporibus vero! Dolorum error, maxime molestiae non quaerat repudiandae tempora unde velit.Aliquid aperiam architecto autem distinctio ducimus illum inventore nulla optio, pariatur perspiciatis possimus praesentium quia quod quos reprehenderit voluptates voluptatum. Adipisci culpa eos libero maxime odio omnis perferendis possimus saepe.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa eos eveniet, ex, facilis laudantium libero omnis perspiciatis, quam quasi quibusdam quis quod sunt voluptate. Dignissimos error eum harum id molestias."},
    {
        imgSrc: "img/graphicDesign/graphic-design10.jpg",
        title: "seo_services",
        text: " Seo Services  , sapiente veritatis.Adipisci at blanditiis deleniti quam quasi quis repellat reprehenderit ullam! Beatae ipsa, modi molestiae obcaecati placeat quod quos temporibus vero! Dolorum error, maxime molestiae non quaerat repudiandae tempora unde velit.Aliquid aperiam architecto autem distinctio ducimus illum inventore nulla optio, pariatur perspiciatis possimus praesentium quia quod quos reprehenderit voluptates voluptatum. Adipisci culpa eos libero maxime odio omnis perferendis possimus saepe.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa eos eveniet, ex, facilis laudantium libero omnis perspiciatis, quam quasi quibusdam quis quod sunt voluptate. Dignissimos error eum harum id molestias."
    }
];


const _GALLERY_ALL_ITEM = [..._GALLERY_ITEM[1], ..._GALLERY_ITEM[2],..._GALLERY_ITEM[3],..._GALLERY_ITEM[4]]


// main js code

let isNeedLoad = true;
let openedNavIndex = 0;
let amountOfGalleryItems = 12;
const parentGalleryNav = document.getElementById('gallery-navbar-list');

parentGalleryNav.addEventListener('click', event => {
    let pressedItem = event.target;
    toggleGallery(pressedItem.parentElement.children, pressedItem);

});
let  placeToAddWholeContainer = undefined;
const addMoreBtn = document.querySelector('.load-more-btn');

addMoreBtn.remove() ;
//
 placeToAddWholeContainer = document.createElement('div');
 placeToAddWholeContainer.classList.add('gallery-items-container');
 document.querySelector('.work-gallery-section').append(placeToAddWholeContainer);

//

let openedGalleryIndex = null;

showAllGallery(amountOfGalleryItems);
function showAllGallery ( amountOfElement  ) {

    for (let k = 0; k < amountOfElement; k++) {


        const {imgSrc, title, category} = _GALLERY_ALL_ITEM[Math.floor(Math.random() * (_GALLERY_ALL_ITEM.length - 1))],
            container = document.createElement('div'),
            img = document.createElement('img');



        container.classList.add('gallery-item');

        img.src = imgSrc;
        container.dataset.category = category;

        container.addEventListener('mouseenter', e => {
            const hoverContainer = document.createElement('div'),
                hoverIconShare = document.createElement('img'),
                hoverIconSearch = document.createElement('img'),
                hoverTitle = document.createElement('h4'),
                hoverCategory = document.createElement('p'),
                hoverImgContainer = document.createElement("div");

            hoverContainer.classList.add('gallery-hover-wrapper');
            hoverIconShare.classList.add('gallery-hover-img');
            hoverIconSearch.classList.add('gallery-hover-img');
            hoverTitle.classList.add('gallery-hover-title');
            hoverCategory.classList.add('gallery-hover-category');
            hoverImgContainer.classList.add('gallery-hover-img-container')

            hoverIconSearch.src = 'img/gallery-serach-icon.png';
            hoverIconShare.src = 'img/gallery-share-icon.png';
            hoverTitle.innerText = title;
            hoverCategory.innerText = category;

            hoverImgContainer.append(
                hoverIconShare,
                hoverIconSearch
            );

            hoverContainer.append(
                hoverImgContainer,
                hoverTitle,
                hoverCategory
            );
            container.append(hoverContainer);
        });
        container.addEventListener('mouseleave', e => {
            container.querySelector('.gallery-hover-wrapper').remove();
        });
        container.append(
            img);

        placeToAddWholeContainer.append(container)
    }



 if (isNeedLoad) {
     document.querySelector('.work-gallery-section').append(addMoreBtn);
}else {
     addMoreBtn.remove();
 }

}

function toggleGallery(allNavItem, pressedItem) {

    if (placeToAddWholeContainer !== undefined) {
        placeToAddWholeContainer.remove();
    }

    placeToAddWholeContainer = document.createElement('div');
    placeToAddWholeContainer.classList.add('gallery-items-container');
    document.querySelector('.work-gallery-section').append(placeToAddWholeContainer);
    openedGalleryIndex = [...allNavItem].indexOf(pressedItem);


    for (let j = 0; j < allNavItem.length; j++) {
        if (j !== openedGalleryIndex) {
            allNavItem[j].classList.remove('accccc');
        } else {
            allNavItem[j].classList.toggle('accccc');
        }
    }

    if (openedGalleryIndex !== 0 ) {
        addMoreBtn.remove();
    } else if (openedGalleryIndex == 0 ) {
        isNeedLoad = true;
        showAllGallery(amountOfGalleryItems);

    }







    if ( pressedItem !== openedGalleryIndex && openedGalleryIndex !== 0){

        isNeedLoad = false;
        showAllGallery( _GALLERY_ITEM[pressedItem.value].length);
    }
}


addMoreBtn.addEventListener("click" , (event) => {

    if (isNeedLoad) {
        isNeedLoad = false;
        showAllGallery(_GALLERY_ALL_ITEM.length);

    }
})



// part of service

let openedItemIndex = null;
let parenttt = document.getElementById('service-bar-list');


function toggleArticle(allNavItems, pressedItem) {
    openedItemIndex = [...allNavItems].indexOf(pressedItem);
    let articleTextElem = document.getElementById('article-text');
    let articleImg = document.getElementById('article-img');
    articleTextElem.innerHTML = _ARTICLE_CONTENT[openedItemIndex].text;
    articleImg.src = _ARTICLE_CONTENT[openedItemIndex].imgSrc;


    for (let i = 0; i < allNavItems.length; i++) {
        if (i !== openedItemIndex) {
            allNavItems[i].classList.remove('active');
        } else {
            allNavItems[i].classList.toggle('active');
        }
    }
}


parenttt.addEventListener('click', event => {
    let pressedItem = event.target;
    toggleArticle(pressedItem.parentElement.children, pressedItem);
});